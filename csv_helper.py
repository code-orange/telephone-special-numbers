import csv
import os


def get_special_numbers(iso: str = "de"):
    filepath = os.path.join(os.path.dirname(__file__), "content", iso, "numbers.csv")
    numbers = list()

    with open(filepath, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            numbers.append(int(row["number"]))

    return numbers
